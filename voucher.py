# This file is part of the sale_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval


class VoucherLine(metaclass=PoolMeta):
    __name__ = 'account.voucher.line'

    sale_contract = fields.Many2One('sale.contract', 'Contract', domain=[
        ('party', '=', Eval('party'))
    ])

    # @fields.depends('sale_contract', 'analytic_accounts')
    # def on_change_sale_contract(self):
    #     if self.sale_contract and not self.analytic_accounts:
    #         if self.sale_contract.product_lines[0].analytic_account:
    #             to_create = {
    #                 'account': self.sale_contract.product_lines[0].analytic_account.id,
    #                 'root': 1,
    #             }
    #             self.write([self], {'analytic_accounts': [('create', [to_create])]})

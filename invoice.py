# This file is part of the sale_weight module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval
from trytond.wizard import Wizard
from trytond.transaction import Transaction

MONTH_ = ['', 'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
          'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE',
          'DICIEMBRE']


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    utility_reading = fields.Many2One('utility_water.utility_reading',
        'Reading', readonly=True, states={
            'invisible': (Eval('type') != 'out'),
        })
    balance = fields.Function(fields.Numeric('Balance'), 'get_balance')
    balance_before_invoice = fields.Function(fields.Numeric('Balance Before Invoice'),
        'get_balance_before_invoice')
    subsidy = fields.Numeric('Subsidy', digits=(16,2))

    @classmethod
    def post(cls, invoices):
        super(Invoice, cls).post(invoices)
        for inv in invoices:
            account_inv = inv.account
            if inv.move:
                move = inv.move
                for lm in move.lines:
                    for al in lm.analytic_lines:
                        lines = lm.search([
                            ('account', '=', account_inv.id),
                            ('move', '=', move.id)
                            ])
                        if lines:
                            al.debit = al.credit
                            al.credit = 0
                            al.move_line = lines[0].id
                            al.save()
                            lm.save()
                            break

    def get_total_amount_with_balance(self):
        return self.total_amount + self.balance_before_invoice

    def get_period(self, number):
        if number:
            return MONTH_[number]
        return ''

    def get_balance(self, name):
        balance = 0
        sale = self.sales[0] if self.sales else None
        if sale and sale.contract_line:
            contract = sale.contract_line.contract
            balance = contract.collection_amount
        return balance

    def get_balance_before_invoice(self, name):
        balance = self.balance
        if self.move and balance != 0:
            balance = balance - self.total_amount
        return balance

    def get_last_payment(self):
        date_last_pay = None
        Voucher = Pool().get('account.voucher')
        sale = self.sales[0] if self.sales else None
        dom = [
            ('party', '=', self.party.id)
        ]
        if sale and sale.contract_line:
            contract = sale.contract_line.contract
            dom.append(
                ('lines.sale_contract', '=', contract.id)
            )
        vouchers = Voucher.search([dom])
        if vouchers:
            date_last_pay = max([v.date for v in vouchers])
        return date_last_pay
